#include <sys/time.h>
#include <unistd.h>

#include "phonics_GrpcEvalClient.h"
#include "phonics_Env.h"
#include "phonics_Log.h"
#include "phonics_ResultCode.h"

/* ######################################################################################################### */

int writeSPK2UTT(string dir);
int writeUTT2SPK(string dir);
int writeWAVSCP(string dir, string filepath);
int writeTEXT(string dir, string text);
int checkPron(string word, string chk_sym, string pron) ;

/* ######################################################################################################### */

void execEvaluationThread(int trans_idx)
{
    int                         rcode = 0;
    GrpcEvalClient *            client = g_env->trans.get(trans_idx);
    Response_EnglishPronEval    reply_;
    struct timeval              begin_time;
    struct timeval              end_time;

	gettimeofday(&begin_time, NULL);

    g_log->info("--> RECEIVED MESSAGE");
    g_log->info("    * trans_id = {}", client->request_.trans_id());
    g_log->info("    * filename = {}", client->request_.filename());
    g_log->info("    * answer_text = {}", client->request_.answer_text());
    g_log->info("    --------------------------------------------------------------------");

    reply_.set_trans_id(client->request_.trans_id());
    if(client == NULL) {
        g_log->error("    * result_code = {} >> internal error >> not found in trans table\n");
        reply_.set_result_code(static_cast<int>(ResultCode::fail));
    }
    else {
        rcode = execEval(client->request_, reply_);
        reply_.set_result_code(rcode);

        if(rcode >= 0) {
            g_log->info("    * status = [{}]", reply_.status());
            g_log->info("    * user_pron = [{}]", reply_.user_pron());
        }

        g_log->info("    * result_code = {}", reply_.result_code());
    }

    client->getResponder()->Finish(reply_, Status::OK, client);

    gettimeofday(&end_time, NULL);
    long dur = (end_time.tv_sec%1000*1000 + end_time.tv_usec/1000) - (begin_time.tv_sec%1000*1000 + begin_time.tv_usec/1000);
    g_log->info("    * duration = {:.3f}s\n", dur/1000.0);

    g_log->flush();
}

int procEval(&reply_, GrpcEvalClient *client) 
{
}

/* ############################################################################### */

int execEval(PhonicsRequest &param, PhonicsResponse &res)
{
    uuid_t uuid;
    char uuid_str[40];
    uuid_generate(uuid);
    uuid_unparse_lower(uuid, uuid_str);

    string base_dir = gHomePath + "/workspace/";
    string output_dir;
    string data_dir;

    string cmd;
    string pron;

    vector<string>  tokens;

    string answer_text = param.answer_text();
    string chk_sym = param.chk_sym();
    transform(answer_text.begin(), answer_text.end(), answer_text.begin(), ::toupper);
    transform(chk_sym.begin(), chk_sym.end(), chk_sym.begin(), ::toupper);
    /* transform(param.answer_text().begin(), param.answer_text().end(), param.answer_text().begin(), toupper); */

    cout << endl;
    cout << "@reqest : " << answer_text << ", " << chk_sym << ", " << param.filename() << endl;


    /* 환경 구축 */
    {
        /* 출력 폴더 생성 */
        output_dir = base_dir + "out." + uuid_str;
        mkdir(output_dir.c_str(), 0777);

        /* 입력 데이타 폴더 생성 */
        data_dir = base_dir + "data." + uuid_str;
        mkdir(data_dir.c_str(), 0777);
        writeSPK2UTT(data_dir);
        writeUTT2SPK(data_dir);
        writeTEXT(data_dir, answer_text);
        writeWAVSCP(data_dir, param.filename());
    }

 	/* 분석 실행 */
    {
        char buff[1000];

        /* 작업 폴더로 이동 */
        chdir((gHomePath + "/kaldi/egs/librispeech/phonics").c_str());

        /* 실행 명령 #1 */
        string cmd = string("/usr/bin/bash minds_align.sh ") + data_dir + " lang model " + output_dir;
        FILE *fp = popen(cmd.c_str(), "r");
        if(fp == NULL) {
            LOGE("[{}] can't exec - '{}'", "session", cmd);
            return -1;
        }
        while(fgets(buff, sizeof(buff), fp) != NULL) ; // LOGI("{}", buff);
        fclose(fp);

        /* system(cmd.c_str()); */

        /* 실행 명령 #2 */
        cmd = string("/usr/bin/bash get_prons.sh ") + data_dir + " lang " + output_dir;
        fp = popen(cmd.c_str(), "r");
        if(fp == NULL) {
            LOGE("[{}] can't exec - '{}'", "session", cmd);
            return -1;
        }
        while(fgets(buff, sizeof(buff), fp) != NULL) ; // LOGI("{}", buff);
        fclose(fp);

        /* system(cmd.c_str()); */
    }

 	/* 결과 확인 */
    {
        string line;
        ifstream result_file(output_dir + "/pron_counts_nowb.txt");
        if(result_file.is_open()) {
            getline(result_file, line);
            if(getline(result_file, line) != NULL) {
                split(line, tokens, "\t\r\n ");
                cout << "tokens = " << tokens.size() << endl;
                if(tokens.size() < 3) {
                    res.set_result_code(-1);
                    res.set_user_pron("");
                    res.set_status(0);

                    goto __END;
                }

                for(int xx = 2; xx < tokens.size(); xx++) {
                    if(xx != 2) pron += " ";
                    pron += tokens[xx];
                }

                cout << "Your Pron = " << pron << ", Good? " << checkPron(answer_text, chk_sym, pron) << endl;

                res.set_result_code(200);
                res.set_user_pron(pron);
                res.set_status(checkPron(answer_text, chk_sym, pron));
            }
            else {
                cout << "EVAL FAIL"  << endl;

                res.set_result_code(-1);
                res.set_user_pron("");
                res.set_status(0);
            }
        }
        else {
            cout << "EVAL FAIL"  << endl;

            res.set_result_code(-1);
            res.set_user_pron("");
            res.set_status(0);
        }

    }

__END:

    /* CLEAN-UP */
    {
        /* 입력 데이타 폴더 삭제 */
        system((string("/usr/bin/rm -rf ") + data_dir).c_str());
        cout << "@info : in = " << data_dir << endl;

        /* 출력 폴더 삭제 */
        /* system((string("/usr/bin/rm -rf ") + output_dir).c_str()); */
        cout << "@info : out = " << output_dir << endl;
    }

    return 0;
}


/* ############################################################################### */
//
// 입력 데이타를 위한 파일들 생성
//
//


int writeSPK2UTT(string dir)
{
    system((string("echo 'curSpeaker input' > ") + dir + "/spk2utt").c_str());
    return 0;
}

int writeUTT2SPK(string dir)
{
    system((string("echo 'input curSpeaker' > ") + dir + "/utt2spk").c_str());
    return 0;
}

int writeTEXT(string dir, string text)
{
    system((string("echo 'input ") + text + "' > " + dir + "/text").c_str());
    return 0;
}


int writeWAVSCP(string dir, string filepath)
{
    system((string("echo 'input ") + filepath + "' > " + dir + "/wav.scp").c_str());
    return 0;
}




/* ############################################################################### */
//
// 발음 정답 여부 확인
//

int checkPron(string word, string chk_sym, string pron)
{
    string sql;
    sql::Statement *                stmt = gDBConn->createStatement();
    sql::ResultSet *                res = NULL;
    sql::ResultSet *                res2= NULL;
    int                             count = 0;
    long                            word_id = -1;
    long                            pron_id = -1;
    int                             type = 0;
    int                             correct_flag = 0;

    /* GET - WORD_ID */
    sql = "SELECT word_id FROM phonics_word WHERE word='" + word + "'";
    res = stmt->executeQuery(sql);
    if(res != NULL && res->next()) {
        word_id = res->getInt64(1);
    }
    if(res != NULL) delete res;
    if(word_id < 0) {
        delete stmt;
        return 0;
    }

    /* GET - CORRECT PRON_ID */
    sql = "SELECT pron_id FROM phonics_correct_pron WHERE word_id=" + to_string(word_id) + " AND pron='" + pron + "'";
    res = stmt->executeQuery(sql);
    while(res != NULL && res->next()) {
        pron_id = res->getInt64(1);

        /* GET - CORRECT_FLAG */
        sql = "SELECT correct_flag FROM phonics_chk_symbol WHERE word_id=" + to_string(word_id)
            + " AND type=1 AND pron_id=" + to_string(pron_id) + " AND chk_symbol='" + chk_sym + "'";
        res2 = stmt->executeQuery(sql);
        if(res2 != NULL && res2->next()) {
            correct_flag = res2->getInt(1);
        }
        if(res2 != NULL) delete res2;
        res2 = NULL;

        /* 정답이면 바로 리턴 */
        if(correct_flag == 1) {
            delete stmt;
            return 1;
        }

    }
    if(res != NULL) delete res;

	/* GET - INCORRECT PRON_ID */
    sql = "SELECT pron_id FROM phonics_incorrect_pron WHERE word_id=" + to_string(word_id) + " AND pron='" + pron + "'";
    res = stmt->executeQuery(sql);
    while(res != NULL && res->next()) {
        pron_id = res->getInt64(1);

        /* GET - CORRECT_FLAG */
        sql = "SELECT correct_flag FROM phonics_chk_symbol WHERE word_id=" + to_string(word_id)
            + " AND type=0 AND pron_id=" + to_string(pron_id) + " AND chk_symbol='" + chk_sym + "'";
        res2 = stmt->executeQuery(sql);
        if(res2 != NULL && res2->next()) {
            correct_flag = res2->getInt(1);
        }
        if(res2 != NULL) delete res2;
        res2 = NULL;

        /* 정답이면 바로 리턴 */
        if(correct_flag == 1) {
            delete stmt;
            return 1;
        }

    }
    if(res != NULL) delete res;

    return 0;
}



