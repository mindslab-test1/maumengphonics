#ifndef __phonics_ResultCode_h__
#define __phonics_ResultCode_h__

enum class ResultCode {
    succ = 200,
    fail = -1,
    queue_full = -2,
    not_found = -3,
    out_of_mem = -4,
};


#endif

