#include "phonics_GrpcEvalClient.h"
#include "phonics_Eval.h"
#include "phonics_Env.h"
#include "phonics_ResultCode.h"

void GrpcEvalClient::proceed()
{
    if (status_ == CREATE) {
        // Make this instance progress to the PROCESS state.
        status_ = PROCESS;

        // 요청 대기 등록
       	service_->PhonicsRequest(&ctx_, &request_, &responder_, cq_, cq_, this);
    } else if (status_ == PROCESS) {

        trans_idx = g_env->trans.set(this);
        /* 추가 자원 없음: 에러 반환 */
        if(trans_idx < 0) {
            sendError(ResultCode::queue_full);
        }
        else {
            thr = move(thread(execEvaluationThread, trans_idx));
        }
        status_ = FINISH;

        /* FOR NEXT REQUEST */
        new GrpcEvalClient(service_, cq_);
    } else {
        GPR_ASSERT(status_ == FINISH);

        if(g_env->running_flag == true) g_env->trans.reset(trans_idx);
    }
}

GrpcEvalClient::~GrpcEvalClient()
{
    if(thr.joinable()) thr.join();
}

/* 에러 반환 */
void GrpcEvalClient::sendError(ResultCode code)
{
    PhonicsResponse reply_;

    reply_.set_result_code(static_cast<int>(code));
    getResponder()->Finish(reply_, Status::OK, this);
}
 
