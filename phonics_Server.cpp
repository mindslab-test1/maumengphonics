#include "phonics.d.h"
#include "phonics_Server.h"
#include "phonics_GrpcEvalClient.h"
#include "phonics_Env.h"

phonics_Server::~phonics_Server()
{
    /* 자원 해지 */
    server_->Shutdown();
    cq_->Shutdown();

    /* 쓰레드 종료 대기 */
    if(thr.joinable()) thr.join();
}

void phonics_Server::run()
{
	string server_addr("0.0.0.0:" + to_string(g_conf->grpc_server.port));

	ServerBuilder builder;
	// Listen on the given address without any authentication mechanism.
	builder.AddListeningPort(server_addr, grpc::InsecureServerCredentials());

	// Register "service_" as the instance through which we'll communicate with
	// clients. In this case it corresponds to an *asynchronous* service.
	builder.RegisterService(&service_);

	// Get hold of the completion queue used for the asynchronous communication
	// with the gRPC runtime.
	cq_ = builder.AddCompletionQueue();

	// Finally assemble the server.
	server_ = builder.BuildAndStart();
	std::cout << "Server listening on " << server_addr << std::endl;

	// Proceed to the server's main loop.
	thr = move(thread([&]() {
			handleRpcs();
	}));
}

void phonics_Server::handleRpcs()
{
	new GrpcEvalClient(&service_, cq_.get());
	void *tag = NULL;
	bool ok = false;

	while(true) {
		cq_->Next(&tag, &ok);
		/* 종료 절차중이면 요청 처리를 하지 않는다. */
		if(g_env->running_flag == false) break;

		static_cast<GrpcEvalClient *>(tag)->proceed();
	}
}
    
