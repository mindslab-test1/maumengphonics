#ifndef __UTIL_H__
#define __UTIL_H__

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <ctime>
#include <iterator>
#include <sys/time.h>
#include <unistd.h>

using namespace std;

/* ################################################################################# */

void split(const string& str, vector<string>& tokens, const string& delimiters);
string genFileName(string prefix);
bool checkFile(string path);


/* ################################################################################# */

#endif
