#include "phonics_AppConfig.h"

AppConfig::AppConfig(string cfgpath)
{
    Config cfg;
    try {
        cfg.readFile(cfgpath.c_str());

        Setting &root = cfg.getRoot();
        parseLog(root);
        parseGrpcServer(root);
        parseMysql(root);

    } catch( FileIOException &ex) {
        throw ("AppConfig > file I/O error - " + cfgpath).c_str();
    } catch( ParseException &ex) {
        throw string("AppConfig > parsing error").c_str();
    } catch( char const *e) {
        throw e;
    }
}

/* ############################################################################# */

bool checkLevel(string &level) ;
bool checkMode(string &mode);

void AppConfig::parseLog(Setting &root)
{
    try {
        Setting &log_sec = root["log"];

        if(log_sec.lookupValue("module", log.module) == 0) throw "AppConf > not found - 'log.module'";

        if(log_sec.lookupValue("level", log.level) == 0) throw "AppConf > not found - 'log.level'";
        if(checkLevel(log.level) == false) throw "AppConf > invalid value - 'log.level'";

        if(log_sec.lookupValue("mode", log.mode) == 0) throw "AppConf > not found - 'log.mode'";
        if(checkMode(log.mode) == false) throw "AppConf > invalid value - 'log.mode'";

    } catch (SettingNotFoundException &ex) {
        throw "AppConfig > parsing error in 'grpc_server'";
    }
}

bool checkLevel(string &level)
{
    if(!level.compare("debug")) return true;
    if(!level.compare("info")) return true;
    if(!level.compare("warn")) return true;
    if(!level.compare("error")) return true;
    if(!level.compare("critical")) return true;
    if(!level.compare("off")) return true;

    return false;
}

bool checkMode(string &mode)
{
    if(!mode.compare("daily")) return true;

    return false;
}


/* ############################################################################# */

void AppConfig::parseGrpcServer(Setting &root)
{
    try {
        Setting &sec___grpc_server = root["grpc_server"];

        if(sec___grpc_server.lookupValue("port", grpc_server.port) == 0) throw "AppConf: not found - 'grpc_server.port'";
        if(sec___grpc_server.lookupValue("max_thread", grpc_server.max_thread) == 0) throw "AppConf: not found - 'grpc_server.max_thread'";

    } catch (SettingNotFoundException &ex) {
        throw "AppConfig > parsing error in 'grpc_server'";
    }
}

/* ############################################################################# */

void AppConfig::parseMysql(Setting &root)
{
    try {
        Setting &sec___mysql= root["mysql"];

        if(sec___mysql.lookupValue("ip", mysql.ip) == 0) throw "AppConf: not found - 'mysql.ip'";
        if(sec___mysql.lookupValue("port", mysql.port) == 0) throw "AppConf: not found - 'mysql.port'";
        if(sec___mysql.lookupValue("user", mysql.user) == 0) throw "AppConf: not found - 'mysql.user'";
        if(sec___mysql.lookupValue("password", mysql.password) == 0) throw "AppConf: not found - 'mysql.password'";
        if(sec___mysql.lookupValue("schema", mysql.schema) == 0) throw "AppConf: not found - 'mysql.schema'";
        if(sec___mysql.lookupValue("conn_timeout", mysql.conn_timeout) == 0) throw "AppConf: not found - 'mysql.conn_timeout'";

    } catch (SettingNotFoundException &ex) {
        throw "AppConfig > parsing error in 'mysql'";
    }
}

