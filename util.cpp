#include "util.h"
 
using namespace std;
 

/*
** 문자열 분리
*/
void split(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    // 맨 첫 글자가 구분자인 경우 무시
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // 구분자가 아닌 첫 글자를 찾는다
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos) {
        // token을 찾았으니 vector에 추가한다
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // 구분자를 뛰어넘는다.  "not_of"에 주의하라
        lastPos = str.find_first_not_of(delimiters, pos);
        // 다음 구분자가 아닌 글자를 찾는다
        pos = str.find_first_of(delimiters, lastPos);
    }
}

/* #################################################################################################### */

/*
** 날짜시간으로 화일명 생성
*/
string genFileName(string prefix) 
{
    time_t cur_time;
    char str_time[100] = "";
    string filename = "";

    struct timeval tv;
    gettimeofday(&tv, NULL);
    
    time(&cur_time);
    strftime(str_time, sizeof(str_time), "%Y%m%d-%H%M%S", localtime(&cur_time));
    filename = prefix + "." + str_time + "-" + to_string(tv.tv_usec/1000);
    return filename;
}

/* #################################################################################################### */

bool checkFile(string path) 
{
	return ( access( path.c_str(), F_OK ) != -1 );
}

