#include <unistd.h>
#include <poll.h>

#include "phonics.d.h"
#include "phonics_Server.h"
#include "phonics_Env.h"
#include "phonics_Log.h"

/* ############################################################################### */

AppConfig *g_conf = NULL;
Env *g_env = NULL;
phonics_Server *g_server = NULL;
std::shared_ptr<spdlog::logger> g_log;

/* ############################################################################### */

void setSignals();
void procTerm(int signo);
void ignoreSignal(int signo);
int initLog();

/* ############################################################################### */

int main()
{
    /* 패키지 홈 패스 조회 */
    char *home = getenv("PHONICS_HOME");
    if(home == NULL) {
        printf("@error: not found - 'PHONICS_HOME'\n\n");
        procTerm(0);
        return 0;
    }
    chdir(home);


    /* 설정 정보 로딩 */
    try {
        g_conf = new AppConfig(string(home) + "/conf/phonics.cfg");
    } catch(char const *e) {
        printf("@error: %s\n", e);
        procTerm(0);
        return 0;
    }

    /* 환경 객체 생성 */
    try {
        g_env = new Env();
    } catch(char const *e) {
        printf("@error: %s\n", e);
        procTerm(0);
        return 0;
    }


    /* SIGNAL 등록 */
    setSignals();

    /* LOG 설정 */
    if(initLog() < 0) {
        printf("@error: log fail\n");
        procTerm(0);
        return 0;
    }

    /* GRPC 서버 */
    try {
        g_server = new phonics_Server;
        g_server->run();
    } catch(char const *e) {
        printf("@error: %s\n", e);
        procTerm(0);
        return 0;
    }

    /* 대기 */
    while(1) {
        poll(NULL, 0, 100);
    }

    procTerm(0);
    return 0;
}

/* ############################################################################################## */
//
// SIGNAL
// 
/* ---------------------------------------------------------------------------------------------- */

void setSignals()
{
    struct sigaction siga;

    /* 종료 처리할 시그널 등록 */
    siga.sa_handler = procTerm;
    sigfillset(&siga.sa_mask);
    sigaction(SIGINT, &siga, NULL);

    /* 무시할 시그널 등록 */
    siga.sa_handler = ignoreSignal;
    sigemptyset(&siga.sa_mask);
    sigaction(SIGCHLD, &siga, NULL);
    sigaction(SIGHUP, &siga, NULL);
    sigaction(SIGALRM, &siga, NULL);
    sigaction(SIGPIPE, &siga, NULL);
}

/* 종료 처리 */
void procTerm(int signo)
{
    if(g_env != NULL) g_env->running_flag = false;

    // g_env가 할당되어 있으면 로그도 활성화 되어 있을
    if(spdlog::get(string("")) != NULL && signo != 0) LOGI("--> RECEIVED SIGNAL : {}\n", sys_siglist[signo]);

    if(g_server != NULL) delete g_server;
    if(g_conf != NULL) delete g_conf;
    if(g_env != NULL) delete g_env;

    if(g_log != NULL) LOGC("--> STOP");
    if(g_log != NULL) LOGC("######################################################################################\n");
    exit(0);
}

void ignoreSignal(int signo)
{
    LOGI("--> RECEIVED SIGNAL : {}\n", sys_siglist[signo]);
}


/* ############################################################################################## */
//
// LOG
// 
/* ---------------------------------------------------------------------------------------------- */

spdlog::level::level_enum getLogLevel();

/* ---------------------------------------------------------------------------------------------- */

int initLog()
{
    spdlog::set_level(getLogLevel());
    spdlog::set_pattern("[%H:%M:%S.%e] [%L] %v");

    if(g_conf->log.mode.compare("daily") == 0) {
        g_log = spdlog::daily_logger_mt("", string(getenv("PKG_HOME")) + "/log/" + g_conf->log.module, 0, 0);
    }
    else return -1;

    LOGC("######################################################################################");
    LOGC("--> START\n");
    return 0;
}

spdlog::level::level_enum getLogLevel()
{
    if(g_conf->log.level.compare("debug") == 0) return spdlog::level::debug;
    if(g_conf->log.level.compare("info") == 0) return spdlog::level::info;
    if(g_conf->log.level.compare("warn") == 0) return spdlog::level::warn;
    if(g_conf->log.level.compare("error") == 0) return spdlog::level::err;
    if(g_conf->log.level.compare("critical") == 0) return spdlog::level::critical;
    if(g_conf->log.level.compare("off") == 0) return spdlog::level::off;

    return spdlog::level::info;
}

