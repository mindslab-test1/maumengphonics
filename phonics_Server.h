#ifndef __phonics_Server_h__
#define __phonics_Server_h__

#include <iostream>
#include <memory>
#include <string>
#include <pthread.h>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "phonics.grpc.pb.h"
#include "phonics_Transaction.h"

using namespace std;

using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerCompletionQueue;
using grpc::Status;

using engedu::eng_evaluation::PhonicsRequest;
using engedu::eng_evaluation::PhonicsResponse;
using engedu::eng_evaluation::EvaluationService;


/* ########################################################################## */

class phonics_Server {
public:
    phonics_Server() {}
    ~phonics_Server();

    void run();

private:
    void handleRpcs();

private:
    std::unique_ptr<ServerCompletionQueue> cq_;
    EvaluationService::AsyncService service_;
    std::unique_ptr<Server> server_;

    thread thr;
};

#endif

