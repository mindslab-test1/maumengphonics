#ifndef __phonics_GrpcEvalClient_h__
#define __phonics_GrpcEvalClient_h__

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "phonics.grpc.pb.h"
#include "phonics_ResultCode.h"

using namespace std;

using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerCompletionQueue;
using grpc::Status;

using engedu::eng_evaluation::PhonicsRequest;
using engedu::eng_evaluation::PhonicsResponse;
using engedu::eng_evaluation::EvaluationService;


class GrpcEvalClient {
public:

    GrpcEvalClient(EvaluationService::AsyncService* service, ServerCompletionQueue* cq)
        : service_(service), cq_(cq), responder_(&ctx_), status_(CREATE) {

        proceed();
    }
    ~GrpcEvalClient();

    void proceed();
    void sendError(ResultCode code);

    ServerAsyncResponseWriter<PhonicsResponse> *getResponder() { return &responder_; }

public:
    enum CallStatus { CREATE, PROCESS, FINISH };

    thread thr;
    int trans_idx = -1;     // TRANSACTION TABLE INDEX

    PhonicsRequest request_;

private:

    EvaluationService::AsyncService* service_;
    ServerCompletionQueue* cq_;
    ServerContext ctx_;


    ServerAsyncResponseWriter<PhonicsResponse> responder_;

    CallStatus status_;
};


#endif

