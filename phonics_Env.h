#ifndef __phonics_Env_h__
#define __phonics_Env_h__

#include "phonics_AppConfig.h"
#include "phonics_Transaction.h"

class Env {
public:
    Env();
    ~Env();

    Transaction         trans;                                          // GRPC 트랜젝션 관리
    bool                running_flag = true;                            // 기동중인지, 종료 중인지 설정
private:
};

/* ############################################################################################# */

extern Env *g_env;

#endif

