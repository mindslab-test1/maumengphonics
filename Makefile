EXE=phonics.d

HOST_SYSTEM = $(shell uname | cut -f 1 -d_)
SYSTEM ?= $(HOST_SYSTEM)

CXX = g++

# MAUM: grpc, protobuf
CPPFLAGS += -pthread -I/data1/maum/include -I/home/eduai/project/include
CXXFLAGS += -std=c++11 -g
LDFLAGS += -L/data1/maum/lib -lprotobuf -lpthread -lgrpc++ -lgrpc 

# MYSQL
CPPFLAGS += -I/home/maiedu/lib/mysql/include 
LDFLAGS += -L/home/maiedu/lib/mysql/lib64 -lmysqlcppconn -lssl -lcrypto \

# ETC
LDFLAGS += -lconfig++ -luuid
LDFLAGS += 	-Wl,--no-as-needed \
			-Wl,--as-needed \
           	-ldl -lutil -lm 


# GRPC 
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`
PROTO_PATH = ../proto
PROTOC = protoc
vpath %.proto $(PROTO_PATH)

# 
OBJS = 	\
		phonics.pb.o \
		phonics.grpc.pb.o \
		main.o \
		phonics_AppConfig.o \
		phonics_Server.o \
		phonics_GrpcEvalClient.o \
		phonics_Eval.o \
		phonics_Transaction.o \
		phonics_Env.o \
		util.o


# BUILD - ALL
all: $(EXE)

# BUILD - MAIN 
$(EXE): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^


# BUILD - PROTO
%.grpc.pb.cc: %.proto
	$(PROTOC) -I $(PROTO_PATH) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

%.pb.cc: %.proto
	$(PROTOC) -I $(PROTO_PATH) --cpp_out=. $<


# CLEAN
clean:
	rm -f *.o *.pb.cc *.pb.h $(EXE)

