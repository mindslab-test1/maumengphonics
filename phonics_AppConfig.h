#ifndef __phonics_AppConfig_h__
#define __phonics_AppConfig_h__

#include <iostream>
#include <string>
#include <libconfig.h++>

using namespace std;
using namespace libconfig;

/* ########################################################################## */

typedef struct {
    string          module;         // 모듈명
    string          level;
    string          mode;
} log_t;

typedef struct {
    int             port;
    int             max_thread;
} grpc_server_t;

typedef struct {
	string			ip;
    int             port;
	string			user;
	string			password;
	string			schema;
	int				conn_timeout;
} mysql_t;


/* ########################################################################## */

class AppConfig {
public:
    AppConfig(string cfgpath);

    log_t           log;
    grpc_server_t   grpc_server;
    mysql_t			mysql;

private:

    void parseLog(Setting &root);
    void parseGrpcServer(Setting &root);
    void parseMysql(Setting &root);

};

/* ########################################################################## */

extern AppConfig *g_conf;



#endif
